<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objects', function (Blueprint $table) {
            $table->id('objectId');
            $table->string('objectName');
            $table->integer('minLevel');
            $table->enum("quality", ['common', 'uncommon', 'rare', 'epic', 'legendary']);
            $table->enum("category", ['equipment', 'consumable', 'cosmetic', 'skill']);
            $table->string('path')->nullable();
            $table->integer('ownerId')->nullable()->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objects');
    }
}
