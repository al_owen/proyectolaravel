<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Auctions', function (Blueprint $table) {
            $table->id('auctionId');
            $table->integer('auctioneerId')->unsigned()->index();
            $table->integer('objectId')->unsigned()->index();
            $table->integer('maxBidId')->unsigned()->nullable()->index();
            $table->integer('instantPurchase');
            $table->timestamp('expiryTime');
            $table->boolean('auctionInProgress');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Auctions');
    }
}
