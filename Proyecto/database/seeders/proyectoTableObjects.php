<?php

namespace Database\Seeders;

use App\Models\Objects;
use Illuminate\Database\Seeder;

class proyectoTableObjects extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Objects::create([
            'ObjectName' => "Mace of Doom",
            'minLevel' => 5,
            'quality' => "common",
            'category' => "equipment",
            'path' => 'object_maceOfDoom.png',
            'ownerId' => 2,
        ]);

        Objects::create([
            'ObjectName' => "Potion of the Order",
            'minLevel' => 5,
            'quality' => "epic",
            'category' => "consumable",
            'path' => 'object_PotionOfTheOrder.png',
            'ownerId' => 2,
        ]);

        Objects::create([
            'ObjectName' => "Sword of Epsilon",
            'minLevel' => 10,
            'quality' => "rare",
            'category' => "equipment",
            'path' => 'object_swordOfEpsilon.png',
            'ownerId' => 3,
        ]);

        Objects::create([
            'ObjectName' => "Hood of Altair",
            'minLevel' => 15,
            'quality' => "legendary",
            'category' => "cosmetic",
            'path' => 'object_hoodOfAltair.png',
            'ownerId' => 3,
        ]);

        Objects::create([
            'ObjectName' => "dead hard",
            'minLevel' => 4,
            'quality' => "uncommon",
            'category' => "skill",
            'path' => 'object_DeadHard.png',
            'ownerId' => 2,
        ]);
    }
}
