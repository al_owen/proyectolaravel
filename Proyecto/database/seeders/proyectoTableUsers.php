<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class proyectoTableUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'name' => 'Alvaro',
            'email' => 'alvaro@lara.vel',
            'email_verified_at' => now(),
            'password' => Hash::make('super3'),
            'buyer' => 0,
            'gold' => 5000,
            'created_at' => now(),
            'updated_at' => now()

        ]);
        User::create([
            'name' => 'Santi',
            'email' => 'santi@lara.vel',
            'email_verified_at' => now(),
            'password' => Hash::make('super3'),
            'buyer' => 0,
            'gold' => 3000,
            'created_at' => now(),
            'updated_at' => now()

        ]);
        User::create([
            'name' => 'Bernat',
            'email' => 'bernat@lara.vel',
            'email_verified_at' => now(),
            'password' => Hash::make('super3'),
            'buyer' => 1,
            'gold' => 10000,
            'created_at' => now(),
            'updated_at' => now()

        ]);
        User::create([
            'name' => 'Paco',
            'email' => 'Paco@gmail.coma',
            'email_verified_at' => now(),
            'password' => Hash::make('super3'),
            'buyer' => 1,
            'gold' => 500,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        User::create([
            'name' => 'Maria',
            'email' => 'Maria@gmail.coma',
            'email_verified_at' => now(),
            'password' => Hash::make('super3'),
            'buyer' => 1,
            'gold' => 750,
            'created_at' => now(),
            'updated_at' => now()

        ]);
        User::create([
            'name' => 'Juan',
            'email' => 'Juan@gmail.coma',
            'email_verified_at' => now(),
            'password' => Hash::make('super3'),
            'buyer' => 1,
            'gold' => 7000,
            'created_at' => now(),
            'updated_at' => now()

        ]);
        User::create([
            'name' => 'Ana',
            'email' => 'Ana@gmail.coma',
            'email_verified_at' => now(),
            'password' => Hash::make('super3'),
            'buyer' => 0,
            'gold' => 5000,
            'created_at' => now(),
            'updated_at' => now()

        ]);
    }
}
