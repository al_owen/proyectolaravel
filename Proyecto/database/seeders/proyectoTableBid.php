<?php

namespace Database\Seeders;

use App\Models\Bid;
use Illuminate\Database\Seeder;

class proyectoTableBid extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Bid::create([
            'priceOffered' => 150,
            'bidderId' => 4,
            'bidDate' => date("Y-m-d H:i:s", mktime( 7, 10, 5, 03, 01, 2021)),
        ]);

        Bid::create([
            'priceOffered' => 80,
            'bidderId' => 5,
            'bidDate' => date("Y-m-d H:i:s", mktime( 8, 10, 5, 03, 01, 2021)),
        ]);

        Bid::create([
            'priceOffered' => 100,
            'bidderId' => 6,
            'bidDate' => now(),
        ]);
        
        Bid::create([
            'priceOffered' => 300,
            'bidderId' => 4,
            'bidDate' => now(),
        ]);

    }
}
