<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Auctions;
use Illuminate\Database\Seeder;

class proyectoTableAuctions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Auctions::create([
            'auctioneerId' => 2,
            'objectId' => 1,
            'maxBidId' => 1,
            'instantPurchase' => 200,
            'expiryTime' => date("Y-m-d H:i:s", mktime( 12, 10, 5, 03, 01, 2021)),
            'auctionInProgress' => 1
        ]);

        Auctions::create([
            'auctioneerId' => 3,
            'objectId' => 2,
            'maxBidId' => 2,
            'instantPurchase' => 500,
            'expiryTime' => date("Y-m-d H:i:s", mktime( 14, 5, 5, 03, 05, 2021)),
            'auctionInProgress' => 1
        ]);

        Auctions::create([
            'auctioneerId' => 3,
            'objectId' => 3,
            'maxBidId' => 3,
            'instantPurchase' => 600,
            'expiryTime' => date("Y-m-d H:i:s", mktime( 10, 15, 5, 03, 06, 2021)),
            'auctionInProgress' => 1
        ]);

        Auctions::create([
            'auctioneerId' => 3,
            'objectId' => 4,
            'maxBidId' => 4,
            'instantPurchase' => 800,
            'expiryTime' => date("Y-m-d H:i:s", mktime( 15, 00, 5, 03, 06, 2021)),
            'auctionInProgress' => 1
        ]);
    }
}
