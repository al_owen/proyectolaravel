<?php

use App\Http\Controllers\AuctionController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('dashboard');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::patch('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::patch('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'App\Http\Controllers\PageController@index']);
});

Route::group(['prefix' => 'market'], function () {
	Route::get('/home', [AuctionController::class, "marketRoot"]);
	Route::get('/auctions', [AuctionController::class, "auctions"]);
	Route::get('/bids', [AuctionController::class, "bids"]);
	Route::get('/check', [AuctionController::class, "check"]);
	Route::get('/myObjects', [AuctionController::class, "myObjects"]);

	Route::get('/auction', [AuctionController::class, "makeAuction"]);
	Route::post('/auction', [AuctionController::class, "auction"]);

	Route::get('/bid/{id}', [AuctionController::class, "makeBid"]);
	Route::post('/bid/{id}', [AuctionController::class, "bid"]);

	Route::get('/myObjects/newObject', [AuctionController::class, "newObject"]);
	Route::post('/myObjects/newObject', [AuctionController::class, "createObject"]);
});
