<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    use HasFactory;

    protected $table = 'Bid';
    protected $primaryKey = 'bidId';
    protected $fillable = ['bidId', 'priceOffered','bidderId'];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'bidDate' => 'datetime',
    ];

    //many to many solo si es comprador y vendedor al mismo tiempo y si no many to one

    public function auction()
    {
        return $this->belongsTo(Auctions::class, 'auctionId');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userId');
    }
}
