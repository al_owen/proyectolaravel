<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Auctions extends Model
{
    use HasFactory;

    protected $table = 'Auctions';
    protected $primaryKey = 'auctionId';
    protected $fillable = ['auctionId', 'auctioneerId', 'objectId', 'maxBidId', 'instantPurchase', 'expiryTime', 'auctionInProgress'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'auctionInProgress' => 'boolean',
        'expiryTime' => 'datetime',
    ];

    public function object()
    {
        return $this->belongsTo(Objects::class, 'objectId');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userId');
    }

    public function bid()
    {
        return $this->hasOne(Bid::class, 'bidId');
    }
}
