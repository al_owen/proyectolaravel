<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Objects extends Model
{
    use HasFactory;
    protected $table = 'Objects';
    protected $primaryKey = 'objectId';
    protected $fillable = ['objectId', 'objectName', 'minLevel', 'quality', 'category', 'path', 'ownerId'];

    public function user()
    {
        return $this->belongsTo(User::class, 'userId');
    }

    public function auction()
    {
        return $this->hasMany(Auctions::class);
    }
}
