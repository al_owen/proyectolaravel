<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\models\Objects;
use App\models\User;
use App\models\Bid;
use App\models\Auctions;
use Carbon\Carbon;

class AuctionController extends Controller
{

    //esta funcion nos lleva a la vista home
    public function marketRoot()
    {
        $currentUser = Auth::user();
        $objects = Objects::All();
        $auctions = Auctions::where('auctionInProgress', '!=', 0)->get(); //nos devuelve todas las subastas en progreso
        $bids = Bid::all();
        return view('home', compact('auctions'), compact('currentUser'), compact('objects'), compact('bids'));
    }

    public function auctions()
    {
        $userId = Auth::id();
        $currentUser = Auth::user();
        $myauctions = Auctions::where('auctioneerId', '=', $userId)->get(); //nos devuelve todas las subastas de el usuario actual
        return view('auctions', compact('currentUser'), compact('myauctions'));
    }

    public function bids()
    {
        $userId = Auth::id();
        $currentUser = Auth::user();
        $mybids = Bid::where('bidderId', '=', $userId)->get(); //nos devuelve todas las pujas hechas por el usuarion
        return view('bids', compact('currentUser'), compact('mybids'));
    }

    public function makeAuction()
    {
        $userId = Auth::id();
        $myObjects = Objects::where('ownerId', '=', $userId)->get(); //nos devuelve todos los objetos del usuario 
        return view('auction', compact('myObjects'));
    }

    public function auction(Request $request)
    {
        $object = Objects::where('objectName', '=', $request->object)->first(); //nos devuelve el objeto que hayamos seleccionado en el formulario
        $auctions = Auctions::all();
        $canAuct = true;
        foreach ($auctions as $auct) {
            if ($auct->objectId == $object->objectId) {  //verificamos que el objeto no este siendo subastado 
                $canAuct = false;
            }
        }
        if ($canAuct) { //si no esta siendo subastado
            $user = Auth::user();
            if (($user->gold > 5)) { //verificamos que el usuario tenga suficiente dinero
                $date = Carbon::now()->addDay($request->expiration); //añadimos los dias

                $userId = Auth::id();
                $newAuction = new Auctions();  //creamos la subasta y declaramos los valores
                $newAuction->auctioneerId = $userId;
                $newAuction->objectId = $object->objectId;
                $newAuction->maxBidId = null;
                $newAuction->instantPurchase = $request->price;
                $newAuction->expiryTime = $date;
                $newAuction->auctionInProgress = 1;
                $user->gold -= 5; //restamos los 5 oros
                $user->update();
                $newAuction->save();
                return $this->auctions();
            } else {
                $msg = "you don't have enough gold to auct this item";
                return view('message', compact('msg')); //nos envia a la vista de mensajes donde informamos al usuario que el usuario no tiene suficiente oro para hacer la subasta
            }
        } else { //si esta siendo subastado 
            $msg = "The object is already being auctioned";
            return view('message', compact('msg'));  //nos envia a la vista de mensajes donde informamos al usuario que el objeto ya esta siendo subastado
        }
    }

    public function makeBid($id)
    {
        $object =  Objects::where('objectId', '=', $id)->first(); //cogemos el objeto al que queremos pujar
        $auction = Auctions::where('objectId', '=', $id)->get(); //nos devuelve las subastas 
        $currentUser = Auth::user();
        if ($object != null) {
            return view('bid', compact('currentUser'), compact('object'));  //nos envia a la vista de las pujas
        } else {
            $msg = "The object does not exist";
            return view('message', compact('msg')); //nos envia a la vista de de mensajes donde informamos al usuario que el objeto no existe
        }
    }

    public function bid(Request $request, $id)
    {
        $currentUser = Auth::user();
        $userId = $currentUser->userId;
        $object =  Objects::where('objectId', '=', $id)->first(); //cogemos el objeto al que queremos pujar
        $auction = Auctions::where('objectId', '=', $id)->first(); //cogemos la subasta en la que se encuentra
        if ($object != null && $auction != null && $auction->auctionInProgress) {
            if ($request->goldQuantity <= $currentUser->gold) {
                $bids = Bid::where('bidId', '=', $auction->maxBidId)->first(); //cogemos la puja actual mas alta
                if ($bids != null) //si alguien a pujado 
                {
                    $previousBid = $bids; //guardamos la ultima puja
                    if ($previousBid->priceOffered <= $request->goldQuantity) { //verificamos que la ultima puja es menor que la actual
                        $newBid = new Bid(); //creamos la puja
                        $newBid->priceOffered = $request->goldQuantity;
                        $newBid->bidderId = $userId;
                        $newBid->bidDate = now();
                        $newBid->save();
                        if ($auction->instantPurchase < $request->goldQuantity) { //si ofrecemos mas oro que la compra instantanea  
                            $auction->auctionInProgress = 0; //cerramos la subasta
                            $object->ownerId = $userId; //nos asignamos como dueños del objeto
                            $auctioneer = User::where('userId', '=', $auction->auctioneerId)->first();
                            $auctioneer->gold += $request->goldQuantity + 5; //pagamos al vendedor y le devolvemos sus 5 oros
                            $auctioneer->update();
                        }
                        $previousBidder = User::where('userId', '=', $previousBid->bidderId)->first();
                        $previousBidder->gold += $previousBid->priceOffered; //devolvemos el dinero al anterior pujador
                        $currentUser->gold -= $request->goldQuantity; //nos descontamos el oro
                        $previousBid->delete();
                        $auction->maxBidId = $newBid->bidId; //asignamos esta como la puja mas alta
                        $auction->save();
                        $object->update();
                        $currentUser->update();
                        return $this->bids();
                    } else {
                        $msg = "Raise your bid";
                        return view('message', compact('msg')); //nos envia a la vista de de mensajes donde informamos al usuario que la puja es muy baja para efectuarse
                    }
                } else {
                    $newBid = new Bid(); //creamos la puja
                    $newBid->priceOffered = $request->goldQuantity;
                    $newBid->bidderId = $userId;
                    $newBid->bidDate = now();
                    $newBid->save();
                    if ($auction->instantPurchase < $request->goldQuantity) { //si ofrecemos mas de lo que pide el subastador lo compramos instantaneamente
                        $auction->auctionInProgress = 0;
                        $object->ownerId = $userId;
                        $auctioneer = User::where('userId', '=', $auction->auctioneerId)->first();
                        $auctioneer->gold += $request->goldQuantity + 5; //pagamos al vendedor y le devolvemos su dinero
                        $auctioneer->update();
                    }
                    $currentUser->gold -= $request->goldQuantity; //nos descontamos el oro
                    $auction->maxBidId = $newBid->bidId; //asignamos esta como la puja mas alta
                    $object->update();
                    $auction->update();
                    $currentUser->update();
                    return $this->bids();
                }
            } else {
                $msg = "Not enough money to bid";
                return view('message', compact('msg')); //informamos al usuario que el dinero es insuficiente para hacer la puja
            }
        } else {
            $msg = "The object doesn't exist or is not being auctioned";
            return view('message', compact('msg')); //informamos que el objeto no existe o no esta siendo subastado
        }
    }



    public function check()
    {
        $currentUser = Auth::user();
        $tiempoActual = now();
        $allAuctions = Auctions::where('expiryTime', '>', $tiempoActual)->get();
        $auctions = Auctions::where('expiryTime', '<', $tiempoActual)->get(); //cojemos todas las subastas que ya hayan expirado
        if ($auctions != null) {
            foreach ($auctions as $auct) {
                if ($auct->auctionInProgress) { //verifica que aun indiquen que estan en progreso
                    $bid = $auct->bid;
                    $bidder = User::where('userId', '=', $bid->bidderId)->first();
                    $auct->auctionInProgress = 0; //indicamos que ya no esta en progreso
                    $comision = $bid->priceOffered * 0.05;
                    $devolucion = $bid->priceOffered - $comision; //calculamos la devolucion descontando la comision
                    $bidder->gold += $devolucion; //devolvemos el dinero
                    $auct->update();
                    $bidder->update();
                }
            }
        }
        return view('check', compact('auctions'), compact('allAuctions'), compact('currentUser'));
    }

    public function myObjects()
    {
        $userId = Auth::id();
        $currentUser = Auth::user();
        if ($currentUser->buyer) {
            $myObjects = Objects::where('ownerId', '=', $userId)->get(); //nos devuelve los objetos del usuario
        } else {
            $myObjects = Objects::all();
        }
        return view('myObjects', compact('currentUser'), compact('myObjects'));
    }

    public function newObject()
    {
        $categories = ['equipment', 'consumable', 'cosmetic', 'skill']; //indicamos las categorias que puede selecionar el usuario
        $qualities = ['common', 'uncommon', 'rare', 'epic', 'legendary']; //indicamos las calidad que puede escoger el usuario
        return view('newObject', compact('categories'), compact('qualities'));
    }

    public function createObject(Request $request)
    {
        $request->validate([
            'objectImg' => 'required|mimes:png,jpg,gif|max:5120', //la imagen es obligatoria y debe tener como extension png, jpg, gif y tamaño maximo 5MB
        ]);

        $filename = 'object_' . $request->objname . '.' . $request->objectImg->extension();
        $request->objectImg->move(public_path('uploads'), $filename); //guardamos en uploads la imagen

        $newObject = new Objects(); //creamos el objeto y le pasamos los valores establecidos en la vista
        $newObject->objectName = $request->objname;
        $newObject->minLevel = $request->level;
        $newObject->quality = $request->quality;
        $newObject->category = $request->category;
        $newObject->path = $filename;
        $newObject->ownerId = Auth::id(); //nos declaramos como dueños del objeto
        $newObject->save();
        return $this->myObjects();
    }
}
