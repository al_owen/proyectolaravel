@extends('layouts/app', ['activePage' => 'My Objects', 'title' => 'My Objects'])

@section('content')
<body style="background: rgba(95, 41, 6, 0.856);">
    <div class="container">
        @if (Auth::check())
            <div>
                <h4><a href="/market/home">Home</a></h4>
                <h3>{{$currentUser->name}}</h3>
                <p style="color: yellow">{{$currentUser->gold}} Gold</p>
            </div>
            <div>
                @if($currentUser->buyer)
                    <h4>My <a href="/market/bids">bids</a></h4>
                    </div>
                        <h1>My Objects</h1>
                    <div>
                @else
                    <h4>Make an <a href="/market/auction">auction</a></h4>
                    <h4>See my <a href="/market/auctions">auctions</a></h4>
                    <h5>Sell an <a href="/market/myObjects/newObject">item</a></h5>
                    </div>
                        <h1>All Objects</h1>
                    <div>
                @endif
            
                <table class="table">
                    <thead>
                        <tr>
                            <th>Object Name</th>
                            <th>Min level</th>
                            <th>Quality</th>
                            <th>Category</th>
                            <th>Download Image</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($myObjects as $object)
                        <tr>
                            <td>{{$object->objectName}}</td>
                            <td>{{$object->minLevel}}</td>
                            <td>{{$object->quality}}</td>
                            <td>{{$object->category}}</td>
                            <td><a href="{{'/uploads/'.$object->path}}" download>Download</a></td>
                        </tr>    
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <h2>You need to login<a href="/login"> Click here to login</a></h2>
        @endif
    </div>
</body>
@endsection