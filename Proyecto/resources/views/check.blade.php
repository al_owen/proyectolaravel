@extends('layouts/app', ['activePage' => 'Check', 'title' => 'check'])

@section('content')
<body style="background: rgba(95, 41, 6, 0.856);">
    <div class="container">
        @if (Auth::check())
            <div>
                <h4><a href="/market/home">Home</a></h4>
                <h3>{{Auth::user()->name}}</h3>
                <p style="color: yellow">{{Auth::user()->gold}} Gold</p>
            </div>
            <div>
                @if(Auth::user()->buyer)
                    <h4>My <a href="/market/bids">bids</a></h4>
                @else
                    <h4>Make an <a href="/market/auction">auction</a></h4>
                    <h4>See my <a href="/market/auctions">auctions</a></h4>
                @endif    
                </div>
                    <h1>Finished Auctions</h1>
                <div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Object name</th>
                            <th>Max Bid</th>
                            <th>Instant purchase</th>
                            <th>Expiration Time</th>
                        </tr>
                    </thead>
                        <tbody>
                            @foreach ($auctions as $auct)
                            <tr>
                                <td>{{$auct->object->objectName}}</td>
                                <td>{{$auct->maxBidId}}</td>
                                <td>{{$auct->instantPurchase}}</td>
                                <td>{{$auct->expiryTime}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                </table>
                </div>
                    <h1>Unfinished Auctions</h1>
                <div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Object name</th>
                            <th>Max Bid</th>
                            <th>Instant purchase</th>
                            <th>Expiration Time</th>
                            @if (Auth::user()->buyer)
                                <th>Bid</th>
                            @endif
                        </tr>
                    </thead>
                        <tbody>
                            @foreach ($allAuctions as $auct)
                            <tr>
                                <td>{{$auct->object->objectName}}</td>
                                <td>{{$auct->maxBidId}}</td>
                                <td>{{$auct->instantPurchase}}</td>
                                <td>{{$auct->expiryTime}}</td>
                                @if (Auth::user()->buyer)
                                    <td><a href="/market/bid/{{$auct->objectId}}">Bid!</a></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
        @else
            <h2>You need to login<a href="/login"> Click here to login</a></h2>
        @endif
    </div>
</body>
@endsection