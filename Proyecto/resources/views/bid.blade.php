@extends('layouts/app', ['activePage' => 'bid', 'title' => 'bid'])

@section('content')
<body style="background: rgb(95, 41, 6, 0.856);">
    <div>
        <div class="container">
            @if (Auth::check())
                <div>
                    <h4><a href="/market/home">Home</a></h4>
                    <h3>{{$currentUser->name}}</h3>
                    <p style="color: yellow">{{$currentUser->gold}} Gold</p>
                </div>
                @if($currentUser->buyer)
                    <div>
                        <img src="URL::asset('storage/public/uploads/{{$object->path}}')" alt="image" style="width:500px; height: 500px;">
                    </div>   
                    <form method="POST" action="/market/bid/{{$object->objectId}}" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="goldQuantity">How much gold will you bid?</label>
                            <input type="number" name="goldQuantity" class="form-control" placeholder="0">
                        </div>
                        
                        

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Bid</button>
                        </div>
                    {{ csrf_field() }}
                    </form>
                @else
                    <h2>You need to be a buyer <a href="/market/home">back to the market</a></h2>
                @endif
            @else
                <h2>You need to login<a href="/login"> Click here to login</a></h2>
            @endif
        </div>
    </div>
</body>
@endsection