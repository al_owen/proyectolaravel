@extends('layouts/app', ['activePage' => 'Auctions', 'title' => 'Auctions'])

@section('content')
<body style="background: rgb(95, 41, 6, 0.856);">
    <div>
        <div class="container">
            @if (Auth::check())
                <div>
                    <h4><a href="/market/home">Home</a></h4>
                    <h3>{{Auth::user()->name}}</h3>
                    <p style="color: yellow">{{Auth::user()->gold}} Gold</p>
                </div>
                @if(!Auth::user()->buyer)
                    <form method="POST" action="/market/myObjects/newObject" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="text" name="objname" class="form-control" placeholder="Object name" required>
                        </div>
                        <div class="form-group">
                            <input type="number" name="level" class="form-control" placeholder="0" required>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="category">
                                @foreach($categories as $category)
                                    <option value="{{$category}}">{{$category}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="quality">
                                @foreach($qualities as $quality)
                                    <option value="{{$quality}}">{{$quality}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="file" name="objectImg" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    {{ csrf_field() }}
                    </form>
                @else
                    <h2>You need to be a seller <a href="/market/home">back to the market</a></h2>
                @endif
            @else
                <h2>You need to login<a href="/login"> Click here to login</a></h2>
            @endif
        </div>
    </div>
</body>
@endsection