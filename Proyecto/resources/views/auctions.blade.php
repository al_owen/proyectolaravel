@extends('layouts/app', ['activePage' => 'Auctions', 'title' => 'Auctions'])

@section('content')
<body style="background: rgb(95, 41, 6, 0.856);">
    <div>
        <div class="container">
            @if (Auth::check())
                <div>
                    <h4><a href="/market/myObjects">My Objects</a></h4>
                    <h3>{{$currentUser->name}}</h3> <!-- el usuario actual -->
                    <p style="color: yellow">{{$currentUser->gold}} Gold</p> <!-- oro del usuario actual -->
                </div>
                @if(!$currentUser->buyer)
                    <h1>My Auctions</h1>
                        <div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <!--<th>Object name</th>-->
                                        <th>Max Bid</th>
                                        <th>Instant purchase</th>
                                        <th>Expiration Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($myauctions as $auct) <!-- foreach para poder ver todos los items de la lista -->
                                    <tr>
                                        <!--<td>{{$auct->object->objectName}}</td>-->
                                        <td>{{$auct->maxBidId}}</td>
                                        <td>{{$auct->instantPurchase}}</td>
                                        <td>{{$auct->expiryTime}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                    </div>
                @else
                    <h2>You need to be a seller <a href="/market/home">back to the market</a></h2> <!-- si no es un vendedor se le notifica -->
                @endif
            @else
                <h2>You need to login<a href="/login"> Click here to login</a></h2> <!-- si no esta logueado se le notifica -->
            @endif
        </div>
    </div>
</body>
@endsection