@extends('layouts/app', ['activePage' => 'Auction', 'title' => 'Auction'])

@section('content')
<body style="background: rgb(95, 41, 6, 0.856);">
    <div>
        <div class="container">
            @if (Auth::check()) <!-- si hay un usuario logueado podra entrar, si no le comentas que tiene que registrarse o loguearse -->
                <div>
                    <h4><a href="/market/home">Market</a></h4>
                    <h3>{{Auth::user()->name}}</h3>
                    <p style="color: yellow">{{Auth::user()->gold}} Gold</p>
                </div>
                @if(!Auth::user()->buyer)
                    <form method="POST" action="/market/auction" enctype="multipart/form-data">
                        <div class="from-group">
                            <label for="object">Object to sell</label><br>
                            <select class="from-control" name="object">
                                @foreach ($myObjects as $item)<!-- esta en formato desplegable -->
                                    <option value="{{$item->objectName}}">{{$item->objectName}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="from-group">
                            <label for="price">Instant purchase price: </label><br>
                            <input type="number" name="price" class="form-control" placeholder="0" required> <!-- precio para compra instantanea en forma numerica -->
                        </div>
                        <div class="form-group">
                            <label for="expiration">How many days will the auction last?</label>
                            <input type="number" name="expiration" class="form-control" placeholder="0" min="0" max="30" required> <!-- el usuario inserta el numero de dias que quiere que este la puja activa desde el momento que se crea -->
                        </div>
                        <div class="from-group">
                            <button type="submit" class="btn btn-primary">Save</button> <!-- boton para guardar -->
                        </div>
                        {{ csrf_field() }}
                    </form>
                @else
                    <h2>You need to be a seller</h2>
                @endif
            @else
                <h2>You need to login<a href="/login"> Click here to login</a></h2>
            @endif
        </div>
    </div>
</body>
@endsection