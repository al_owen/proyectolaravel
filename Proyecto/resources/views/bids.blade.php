@extends('layouts/app', ['activePage' => 'Bids', 'title' => 'Bids'])

@section('content')
<body style="background: rgb(95, 41, 6, 0.856);">
    <div>
        <div class="container">
            @if (Auth::check())
                <div>
                    <h4><a href="/market/home">Home</a></h4>
                    <h3>{{$currentUser->name}}</h3>
                    <p style="color: yellow">{{$currentUser->gold}} Gold</p>
                </div>
                @if($currentUser->buyer)
                    <h1>My Bids</h1>
                        <div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Object to Win</th>
                                        <th>Price Offered</th>
                                        <th>Bid Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($mybids as $bid)
                                    <tr>
                                        <td>{{$bid->objectName}}</td>
                                        <td>{{$bid->priceOffered}}</td>
                                        <td>{{$bid->bidDate}}</td>
                                    </tr>    
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                @else
                    <h2>You need to be a seller <a href="/market/home">back to the market</a></h2>
                @endif
            @else
                <h2>You need to login<a href="/login"> Click here to login</a></h2>
            @endif
        </div>
    </div>
</body>
@endsection