@extends('layouts/app', ['activePage' => 'home', 'title' => 'Auction Home'])

@section('content')
<body style="background: rgba(95, 41, 6, 0.856);">
    <div class="container">
        @if (Auth::check())
            <div>
                <h4><a href="/market/myObjects">My Objects</a></h4>
                <h3>{{$currentUser->name}}</h3>
                <p style="color: yellow">{{$currentUser->gold}} Gold</p>
            </div>
            <h2>Auctions</h2>
            <div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Object name</th>
                            <th>Max Bid</th>
                            <th>Instant purchase</th>
                            <th>Expiration Time</th>
                            @if ($currentUser->buyer)
                                <th>Bid</th>
                            @endif
                        </tr>
                    </thead>
                        <tbody>
                            @foreach ($auctions as $auct)
                            <tr>
                                <td>{{$auct->object->objectName}}</td>
                                <td>{{$auct->maxBidId}}</td>
                                <td>{{$auct->instantPurchase}}</td>
                                <td>{{$auct->expiryTime}}</td>
                                @if ($currentUser->buyer)
                                    <td><a href="/market/bid/{{$auct->objectId}}">Bid!</a></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
        @else
            <h2>You need to login<a href="/login"> Click here to login</a></h2>
        @endif
    </div>
</body>
@endsection