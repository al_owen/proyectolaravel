@extends('layouts/app', ['activePage' => 'Auctions', 'title' => 'Auctions'])

@section('content')
    <body style="background: rgba(95, 41, 6, 0.856);">
        <div class="container">
            <div>
                <h2>{{$msg}}</h2>
                <h3><a href="/market/home">Home</a></h3>
            </div>
        </div>
    </body>
@endsection